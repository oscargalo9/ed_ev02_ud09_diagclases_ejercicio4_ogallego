package org.cuatrovientos.ed;

public class Invoice {

	private String customer;

	public Product[] listaProductos;

	public Invoice(String customer) {
		super();
		this.customer = customer;
		// Construir mi lista de productos, para evitar NullPointerException
		this.listaProductos = new Product[10];
	}

	public void add(Product p) {
		// TODO; anadir...

		// �Est� mi lista llena?

		if (this.listaProductos[9] == null) {
			// no est� llena
			// tengo que localizar la ultima posici�n vac�a

			// for (Product product : listaProductos) {
			for (int i = 0; i < listaProductos.length; i++) {
				if (this.listaProductos[i] == null) {
					// Agregar producto
					this.listaProductos[i] = p;
					break;
				}
			}

		}
	}

	public void remove(int id) {
		// TODO: eliminar
		this.listaProductos[id] = null;
	}

	public float total() {
		// TODO precio total;
		// return 0.0F;
		float result = 0;
		for (Product product : listaProductos) {
			//Solo sumamos si hay producto
			if (product != null) {
				result += product.total();
			}
			// result = product != null ? result + product.total() : result;			
		}
		return result;
	}
}
